function addMessage(message, severity) {
  return {
    type: 'ADD_MESSAGE',
    message,
    severity
  };
}

function removeMessage() {
  return {
    type: 'REMOVE_MESSAGE'
  };
}

export default { addMessage, removeMessage }