import React, { Component } from 'react';
import { connect } from 'react-redux';
import Messages from '../components/Messages';
import {addMessage,removeMessage} from '../actions/messageQueue';

class App extends Component {
  handleSubmit(e) {
    e.preventDefault();
    let message = React.findDOMNode(this.refs.message);
    if(message.value) {
      let severity = React.findDOMNode(this.refs.severity);
      this.props.dispatch(addMessage(message.value, severity.value))
      message.value = '';
      setTimeout(()=>this.props.dispatch(removeMessage()),5000);
    }
  }

  render () {
    const { messageQueue, dispatch } = this.props;
    return (
      <div>
        <form onSubmit={::this.handleSubmit}>
          <input ref="message" type='text'/>
          <select ref="severity">
            <option>Success</option>
            <option>Info</option>
            <option>Warning</option>
            <option>Danger</option>
          </select>
          <button type="submit">Toast!</button>
        </form>
        <Messages messages={messageQueue} />
      </div>
    );
  }
}

export default connect(state => state)(App);
