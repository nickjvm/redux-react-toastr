import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';
import messageQueue from './reducers/messageQueue';

const createStoreWithMiddleware = applyMiddleware(
  thunkMiddleware,
  loggerMiddleware
)(createStore);

const reducers = combineReducers({messageQueue});

export default function configureStore(initialState) {
  return createStoreWithMiddleware(reducers, initialState);
}