import React from 'react';
import Root from './containers/Root';
import { Provider } from 'react-redux';
import configureStore from './stores/configureStores';

const store = configureStore();

React.render(<Root />, document.getElementById('root'));
