import React, { Component } from 'react/addons';
import '../scss/toastr';

const ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

class Messages extends Component {
  render() {
    let {messages} = this.props;
    let items = messages.map(message => (<li className={message.severity} key={message.timestamp}>{message.text}</li>));

    return (
      <ul>
        <ReactCSSTransitionGroup transitionName="toaster">
          {items}
        </ReactCSSTransitionGroup>
      </ul>
    )
  }
}

export default Messages;
