import Immutable from 'immutable';

export default function messageQueue(state = new Immutable.List(), action) {
  switch (action.type) {
    case 'ADD_MESSAGE':
      return state.unshift({
        severity: action.severity,
        text: action.message,
        timestamp: new Date().getTime()
      });
    case 'REMOVE_MESSAGE':
      return state.pop();
    default:
      return state;
  }
}