redux-react-toastr
=====================

A quick Redux POC using react, hot reloading, immutable, and webpack

### Usage

```
npm install
npm start
open http://localhost:3000
```

### Hot Reloading
Changes to any file in the src directory will appear without reloading the browser like in [this video](http://vimeo.com/100010922).

### Dependencies

* React
* Webpack
* [webpack-dev-server](https://github.com/webpack/webpack-dev-server)
* [babel-loader](https://github.com/babel/babel-loader)
* [react-hot-loader](https://github.com/gaearon/react-hot-loader)
* [immutablejs](https://facebook.github.io/immutable-js)

### Resources

* [Demo video](http://vimeo.com/100010922)
* [react-hot-loader on Github](https://github.com/gaearon/react-hot-loader)
* [Integrating JSX live reload into your workflow](http://gaearon.github.io/react-hot-loader/getstarted/)
* [Troubleshooting guide](https://github.com/gaearon/react-hot-loader/blob/master/docs/Troubleshooting.md)
* Ping dan_abramov on Twitter or #reactjs IRC
